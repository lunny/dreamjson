package dreamjson

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"
)

type item struct {
	key string
	val interface{}
}

type JSON struct {
	data        []item
	indentLevel int
}

func New() *JSON {
	return &JSON{
		data: make([]item, 0, 10),
	}
}

func (j *JSON) SetIndentLevel(indentLevel int) {
	j.indentLevel = indentLevel
}

func (j *JSON) Set(key string, value interface{}) {
	j.data = append(j.data, item{
		key: key,
		val: value,
	})
}

func (j *JSON) Bytes() ([]byte, error) {
	var buf bytes.Buffer
	_, err := j.WriteTo(&buf)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (j *JSON) String() (string, error) {
	var buf strings.Builder
	_, err := j.WriteTo(&buf)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func (j *JSON) WriteTo(w io.Writer) (int, error) {
	var sum int
	n, err := io.WriteString(w, "{")
	sum += n
	if err != nil {
		return sum, err
	}
	if j.indentLevel > 0 {
		io.WriteString(w, "\n")
	}
	var i int
	for _, item := range j.data {
		k := item.key
		v := item.val
		if j.indentLevel > 0 {
			n, err := io.WriteString(w, strings.Repeat("\t", j.indentLevel))
			sum += n
			if err != nil {
				return sum, err
			}
		}
		n, err = fmt.Fprintf(w, `"%s":`, k)
		sum += n
		if err != nil {
			return sum, err
		}
		switch vv := v.(type) {
		case *JSON:
			if j.indentLevel > 0 {
				vv.SetIndentLevel(j.indentLevel + 1)
			}
			n, err := vv.WriteTo(w)
			sum += int(n)
			if err != nil {
				return sum, err
			}
		case io.Reader:
			n, err = io.WriteString(w, `"`)
			sum += int(n)
			if err != nil {
				return sum, err
			}
			n, err := io.Copy(w, vv)
			sum += int(n)
			if err != nil {
				return sum, err
			}
			n2, err := io.WriteString(w, `"`)
			sum += n2
			if err != nil {
				return sum, err
			}
		case int, int64, int32, int8, int16, uint, uint32, uint8, uint16, uint64, float32, float64:
			n, err := fmt.Fprintf(w, "%v", vv)
			sum += int(n)
			if err != nil {
				return sum, err
			}
		case string:
			n, err := fmt.Fprintf(w, `"%v"`, vv)
			sum += int(n)
			if err != nil {
				return sum, err
			}
		default:
			bs, err := json.Marshal(vv)
			if err != nil {
				return sum, err
			}
			n, err := w.Write(bs)
			sum += int(n)
			if err != nil {
				return sum, err
			}
		}
		i++
		if i != len(j.data) {
			n, err = io.WriteString(w, ",")
			sum += int(n)
			if err != nil {
				return sum, err
			}
		}
		if j.indentLevel > 0 {
			io.WriteString(w, "\n")
		}
	}
	if j.indentLevel > 0 {
		n, err := io.WriteString(w, strings.Repeat("\t", j.indentLevel-1))
		sum += n
		if err != nil {
			return sum, err
		}
	}
	n, err = io.WriteString(w, "}")
	sum += n
	return sum, err
}

package dreamjson

import (
	stdjson "encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDreamJSON(t *testing.T) {
	json := New()
	json.SetIndentLevel(1)
	json.Set("a", "b")
	json.Set("b", 1)
	json.Set("c", []int{1, 2})
	json.Set("d", map[string]int{
		"dd": 1,
	})

	json2 := New()
	json2.Set("a2", "b2")
	json2.Set("b2", 1)
	json2.Set("c2", []int{2, 3})
	json.Set("json2", json2)
	json.Set("json3", strings.NewReader("--------------------"))

	var buf strings.Builder
	n, err := json.WriteTo(&buf)
	assert.NoError(t, err)
	fmt.Println(n)
	fmt.Println(buf.String())

	var data = make(map[string]interface{})
	err = stdjson.Unmarshal([]byte(buf.String()), &data)
	assert.NoError(t, err)
	fmt.Println(data)
}

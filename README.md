# dreamjson

## Features

Dreamjson is a json encode library for Golang. It has two features rather than 
stdandard `encoding/json`. 

- It will keep the order of map data
- It will spend less memory if there are some data will be read from disk file.

## Installation

```
go get gitea.com/lunny/dreamjson
```

## Usage

```go
json := dreamjson.New()
json.Set("a", 1)
json.Set("b", map[string]interface{}{
    "bb": 1,
})
json.Set("c", strings.NewReader("abc"))

json2 := dreamjson.New()
json2.Set("dd", 1)
json.Set("d", json2)

n, err := json.WriteTo(resp)
```